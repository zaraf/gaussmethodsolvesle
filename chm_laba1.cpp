// chm_laba1.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
using namespace std;

//------------------------------VARIABLES-----------------------------------------------
const int matrixSize = 4; //����������� �������

int index_j = 0;  // ������ �������� �������� � �������
int index_i = 0; // ������ �������� �������� � ������
double mnoj = 1; //��������� ����������
double temp, tempp;//��������� ����������
double *solveSLE = NULL; //������� ����
double *r = NULL; //�������
double detA = 1; //����������� �������
double **permutationMatrix = NULL;//������� ��������
double **A = NULL;//�������� �������

//---------------------------------------------------------------------------------------

//------------------------DECLARATION-FUNCTION-------------------------------------------
double **initMatrix(string fileName, double **outputMatrix, int sizeRow, int sizeColumn); //������������� �������
void printMatrix(double **inputMatrix,int sizeRow, int sizeColumn); //����� �������
double **copyMatrix(double **inputMatrix,double **outputMatrix, int sizeRow, int sizeColumn); //����������� ������
double **gaussTriangle(double **matrixCoefficients,double **permutationMatrix);
double *backSolve(double **triangleMatrix,double **permutationMatrix); //�������� ��� �������
double **unitMatrix(double **outputMatrix, int sizeRow, int sizeColumn); //������������� ��������� �������
double *Gauss(double **matrixCoefficients,double *solveSLE); //����� ������ � ������� �������� ��������
double **triangleMatrix(double **inputMatrix); //���������� ������� � ����������������� �������
double **Inverse(double **inputMatrix); //���������� �������� �������
double **extendedMatrix(double **mass,int numDiag); //���������� ����������� �������
void check(double **mass1, double **mass2);//�������� �������� �������
//---------------------------------------------------------------------------------------

//------------------------MAIN-FUNCTION--------------------------------------------------
int _tmain(int argc, _TCHAR* argv[])
{
	solveSLE = new double[matrixSize];
	r = new double[matrixSize];
	double **copyA = NULL;
	double **triangle = NULL;
	double **copyForInv = NULL;
	double **copyForCheck = NULL;

	A = initMatrix("matrix.txt",A,matrixSize,matrixSize+1); //������������� ���� �� �����
	cout<<"\nSource SLE:\n";
	printMatrix(A,matrixSize,matrixSize+1);  //����� ��������� ����

	copyA = copyMatrix(A,copyA,matrixSize,matrixSize+1);
	copyForInv = copyMatrix(A,copyForInv,matrixSize,matrixSize+1);
	copyForCheck = copyMatrix(A,copyForCheck,matrixSize,matrixSize);
	permutationMatrix=unitMatrix(permutationMatrix,matrixSize,matrixSize+1); 

	//������� ����
	solveSLE = Gauss(A,solveSLE);

	cout<<"\n Solve SLE : \n";
	for(int i = 0; i < matrixSize; i++)
	{
		cout<<solveSLE[i]<<endl;
	}
	
	//���������� �������
	for(int i = 0; i < matrixSize; i++)
	{
		temp = 0;
		for(int j = 0; j < matrixSize; j++)
		{
		  temp += copyA[i][j]*solveSLE[j];
		}
		r[i] = temp - copyA[i][matrixSize];
	}

	//����� �������
	cout<<"\n Discrepancy : \n";
	for (int i = 0; i < matrixSize; i++)
	{
		cout.precision(17);
		cout.setf(std::ios::fixed, std:: ios::floatfield );
	cout<<r[i]<<endl;
	}
	//���������� ������������
	triangle = new double *[matrixSize];
    for (int i = 0; i < matrixSize; ++i)
        triangle[i] = new double [matrixSize+1];
	triangle = triangleMatrix(copyA);
		//printMatrix(triangle,matrixSize,matrixSize);

		for(int i = 0; i<matrixSize; i++)
		{
			detA *= triangle[i][i];
		}
		cout.precision(4);
		cout.setf(std::ios::fixed, std:: ios::floatfield );
		cout<<"\nDeterminant : "<<detA<<endl;	

		//���������� �������� �������

	cout<<"\nInverse matrix : \n";
	double **inverseMatrix = NULL;
	inverseMatrix = Inverse(copyForInv);
	printMatrix(inverseMatrix,matrixSize,matrixSize);

	cout<<"\nCheck Inverse Matrix : \n";
	check(copyForCheck,inverseMatrix);
	_getch();
	return 0;
}
//---------------------------------------------------------------------------------------


//------------------------FUNCTION-DEFINITION--------------------------------------------
double **initMatrix(string fileName, double **outputMatrix, int sizeRow, int sizeColumn)
{
fstream fi;//���������� ������
	fi.open(fileName, ios::in); //�������� ����� ��� ����������

 outputMatrix = new double *[sizeRow]; //�������� ������� ���� ����� ��������� ����� �� �����
    for (int i = 0; i < sizeRow; ++i)
        outputMatrix[i] = new double [sizeColumn];
	
	int j,i;
	if (!fi)
      { cerr << "Error!\n"; 
         exit(1);
      }
for(i=0;i<sizeRow;i++)
	{
		for(j=0;j<sizeColumn;j++)
		{
			fi>>outputMatrix[i][j]; //��������� ������ �� ����� � ������
		//	cout<<mass[i][j]<<" ";
		}
		//cout<<endl;
	}
	fi.close();//��������� ����
	return outputMatrix;
}

void printMatrix(double **inputMatrix,int sizeRow, int sizeColumn)
{
	//cout.precision(4);
	for(int i = 0; i < sizeRow; i++)
	{
		for(int j = 0; j < sizeColumn; j++)
		{
			cout.precision(4);
			cout.setf(std::ios::fixed, std:: ios::floatfield );
			cout<<setw(13)<<inputMatrix[i][j]<<" "; //����� ��������� �������
		}
		cout<<endl;
	}
}

double **copyMatrix(double **inputMatrix, double **outputMatrix, int sizeRow, int sizeColumn)
{

outputMatrix = new double *[sizeRow]; //��������� ������ ��� ������� ���� ����� ���������� ������
    for (int i = 0; i < sizeRow; ++i)
        outputMatrix[i] = new double [sizeColumn];
	for(int i = 0; i < sizeRow; i++)
	{
		for(int j = 0; j < sizeColumn; j++)
		{
			outputMatrix[i][j] = inputMatrix[i][j]; //����������� �������
		}
	}
	return outputMatrix;
}

double *Gauss(double **matrixCoefficients, double *solveSLE)//������� ����
{	
	matrixCoefficients=gaussTriangle(matrixCoefficients,permutationMatrix);// ���������� ������� � ������������ ���� (������ ��� �������)
	//����������������� �������
	//cout<<"\nTriangle matrix :\n";
	//printMatrix(matrixCoefficients,matrixSize,matrixSize+1); //����� ����������� �������
	solveSLE = backSolve(matrixCoefficients,permutationMatrix); //�������� ��� �������(���������� �����������)
	return solveSLE;
}

double **gaussTriangle(double **matrixCoefficients,double **permutationMatrix)
{
	double mainElement; //������� �������(�.�. ���������� � ������� �������)
	double **massTemp = NULL;//��������� ������
	massTemp = copyMatrix(matrixCoefficients,massTemp,matrixSize,matrixSize + 1);

	for (int j = 0; j < matrixSize; j++)  /*����� �������� �������� �� ��������*/
	{
		mainElement = abs(massTemp[j][j]);
	   for (int i = j ; i < matrixSize; i++)
	   {
		   if(massTemp[i][j]>=mainElement) 
		   {
			   mainElement = massTemp[i][j];//������� �������
			   index_i = i;
		   }
	
	   }
	   //���� ������� ������� ����� 0 (�.�. ��� �������� ������� = 0), �� ������� ����������� � ������� ������ �� �� ������, ������� �� �������
	   if (mainElement==0)
		   return 0;
	   //���� ����� ������� �������, �� ������ ������ � �������
	   if(index_i!=j)
	   {
		   for(int k = 0; k < matrixSize;k++)
		   {
			   temp = massTemp[k][j];
			   massTemp[k][j] = massTemp[k][index_i];
			   massTemp[k][index_i] = temp;
			   
		   }

		  //������� ��������
		    for(int k1 = 0; k1 < matrixSize; k1++)
		   {
			   tempp = permutationMatrix[k1][j];
			   permutationMatrix[k1][j] = permutationMatrix[k1][index_i];
			   permutationMatrix[k1][index_i] = tempp;
		   }

	   }
	   //�������������� �������
	   for(int k = j + 1; k < matrixSize; k++)
	   {
		   mnoj = massTemp[k][j]/massTemp[j][j];
		   for(int l = j; l < matrixSize + 1; l++)
		   {
			   massTemp[k][l] = massTemp[k][l] - mnoj*massTemp[j][l]; //�� ������� ����������� �������
		   }
	   }
	}

	return massTemp;
}

double *backSolve(double **triangleMatrix,double **permutationMatrix)
{
	//������� ���� (�������� ���)
	double *xTemp = NULL;
	
	xTemp = new double[matrixSize];
	for(int i = 0; i< matrixSize; i++)
	{
		 xTemp[i] = 0;
	}

	for(int i = matrixSize - 1; i >= 0; i--)
	{
		temp = 0;
		for(int j = i + 1; j < matrixSize; j++ )
		{
			temp += triangleMatrix[i][j]*xTemp[j];
		}
		xTemp[i] = (triangleMatrix[i][matrixSize] - temp)/triangleMatrix[i][i]; //�� ������� ������� �����������
	}

	
//��������� ������� �� ������� ��������, ����� �������� ������� � ���������� ����������, ��� ��� �� ����� ��� ���������
//����������� ������� �������� ������	
double *x2=NULL;
x2 = new double[matrixSize];

for(int i = 0; i < matrixSize; i++)
	{
		temp=0;
		for(int j = 0; j < matrixSize; j++)
		{
		  temp += permutationMatrix[i][j]*xTemp[j];
		}
		x2[i] = temp;
	}
	
return x2;
}

double **unitMatrix(double **outputMatrix, int sizeRow, int sizeColumn)
{
	outputMatrix = new double *[sizeRow]; //���������� �������
    for (int i = 0; i < sizeRow; ++i)
        outputMatrix[i] = new double [sizeColumn];
	for(int i = 0; i < sizeRow; i++)
	{
		for(int j = 0; j < sizeColumn; j++)
		{
			outputMatrix[i][j] = 0.0; //�������������� �������� = 0
			outputMatrix[i][i] = 1.0; //������������ �������� = 1
		}
	}
	return outputMatrix;
}

double **triangleMatrix(double **inputMatrix)//���������� ������� � ����������������� ������� ������
{
	double **massTemp = NULL;
	massTemp = copyMatrix(inputMatrix,massTemp,matrixSize,matrixSize);

	 for(int i = 0; i < matrixSize - 1; i++)
{        for(int row = i + 1; row < matrixSize; row++)
        {
            temp = -massTemp[row][i] / massTemp[i][i]; //����� ������
            for(int col = i; col < matrixSize; col++)
			{
                massTemp[row][col] += massTemp[i][col] * temp;
			    
			}
        }
}
	 return massTemp;
}

double **Inverse(double **inputMatrix)
{
	double **inv = NULL; //�������� �������
	double *xTemp = NULL; //��������� ������
	double **massTemp = NULL; // ��������� �������
	inv= new double*[matrixSize];
	for(int i = 0; i < matrixSize; i++)
	{
		inv[i] = new double[matrixSize];
	}

	massTemp= new double*[matrixSize];
	for(int i = 0; i < matrixSize; i++)
	{
		massTemp[i] = new double[matrixSize+1];
	}

	xTemp = new double[matrixSize];
	for(int i = 0; i< matrixSize; i++)
	{
		 xTemp[i] = 0;
	}
	//��� ��������� �������� ������� ����� ������ ��������� AX = E, A - ��������� ������������, X - ���������� ������������, 
	//E - ��������� �������
	for(int i = 0; i < matrixSize; i++)//������� ����
	{
		permutationMatrix=unitMatrix(permutationMatrix,matrixSize,matrixSize+1);//�������������� ������� ��������
		inputMatrix = extendedMatrix(inputMatrix, i); //�������� ����������� �������
		massTemp = gaussTriangle(inputMatrix, permutationMatrix); //�������� � ������������ ����
		xTemp = backSolve(massTemp,permutationMatrix); //�������� ����������� ��������
		for(int j = 0; j < matrixSize; j++)
		{
			inv[j][i] = xTemp[j];
		}
	}
	return inv;
}

double **extendedMatrix(double **mass,int numDiag)
{
	double **extended_matrix=NULL;
extended_matrix = new double *[matrixSize];
    for (int i = 0; i < matrixSize; ++i)
        extended_matrix[i] = new double [matrixSize+1];

	for(int i = 0; i < matrixSize; i++)
	{
		for(int j = 0; j < matrixSize + 1; j++)
		{
		   if(j < matrixSize)
		  extended_matrix[i][j] = mass[i][j];
		  
		   if(j == matrixSize && i == numDiag)
			   extended_matrix[i][matrixSize]=1.0;
					else
						extended_matrix[i][matrixSize]=0.0;
		}
	}
	return extended_matrix;
}

void check(double **mass1, double **mass2)//�������� �������� �������
{
	double **Check = NULL;
	Check = new double *[matrixSize];
    for (int i = 0; i < matrixSize; ++i)
       Check[i] = new double [matrixSize];
	
	
	for(int i = 0; i < matrixSize; i++)
	{
		for(int j = 0; j < matrixSize; j++)
		{
			double sum = 0;
			for(int k = 0; k < matrixSize; k++)
			{
				sum+=mass1[i][k]*mass2[k][j]; //������� ��������� ���������
			}

			Check[i][j] = sum;
			//cout.precision(4);
		    //cout.setf(std::ios::fixed, std:: ios::floatfield );
			cout<<setw(14)<<Check[i][j]<<" ";
		}
		cout<<endl;
	}
}
//---------------------------------------------------------------------------------------
